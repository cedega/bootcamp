#include "Engine.h"

Engine::Engine()
{
    initWindow("Neru",800,600);
    Bullet::init();
    player_.load();
    fairy1_.load();
}
    
void Engine::logic(const float dt)
{
    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
        if (event.type == SDL_QUIT)
        {
            ne::Engine::kill();
        }
    }
    
    const Uint8 *keyboard = SDL_GetKeyboardState(NULL);
    
    if (keyboard[SDL_SCANCODE_ESCAPE])
        ne::Engine::kill();
    
    player_.input(dt);
    player_.logic(dt);
    player_.update(dt);
    fairy1_.AI(player_, dt);
    fairy1_.logic(dt);
    fairy1_.update(dt);
    
    player_.updateBullets(dt);
    fairy1_.updateBullets(dt);
}

void Engine::render(ne::Renderer &r)
{
    r.draw(player_);
    r.draw(fairy1_);
    r.draw(player_.getRect(), ne::toSDLColor(0,0,0));
    r.draw(fairy1_.getRect(), ne::toSDLColor(0,0,0));

    player_.drawBullets(r);
    fairy1_.drawBullets(r);
}
