#include "Bullet.h"

ne::Image Bullet::player_bullet_;

Bullet::Bullet(const ne::Vector2f &position, const ne::Vector2f &direction, float lifetime)
{
    load();
    
    setPosition(position - ne::Vector2f(data_.getW()/2.0, data_.getH()/2.0));
    ne::EntityImage::updateLast();
    
    ne::EntityLogic::setGravity(0);
    setSpeed(600.0f);
    
    ne::Entity::input(direction);
    
    data_.setRotation(90.0f + RADTODEG*std::atan2(direction.y, direction.x));
    
    lifetime_ = lifetime;
    alive_ = true;
    tick_ = 0.0f;
}

void Bullet::load()
{
    data_.setTexture(&player_bullet_);
    data_.setSpriteSheet(64, 64, 0);
    
    data_.newAnimation("idle", 1, 1, 100);
    
    ne::EntityLogic::setRect(ne::Rectf(0,0,32,32));
    
    data_.setBaseDisplacement(-10, -10, 32);
    ne::EntityImage::attach(&data_);
    
    loadAnimation("idle");
}

void Bullet::init()
{
    player_bullet_.loadFileImage("bullet.png");
}

void Bullet::update(const float dt)
{
    tick_ += dt;
    
    if (tick_ >= lifetime_)
        alive_ = false;
        
    ne::Entity::update(dt);
}