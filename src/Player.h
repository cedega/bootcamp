#pragma once
#include <Neru/Entity.h>
#include "Bullet.h"
#include <vector>
#include <Neru/Renderer.h>

class Player : public ne::Entity
{
public:
    Player();
    virtual ~Player();
    void load();

    void transitionAnimations();
    void input(const float dt);
    
    void shoot();
    void updateBullets(const float dt);
    void drawBullets(ne::Renderer& r);
    
private:
    ne::EntityData data_;
    std::vector<Bullet*> bullets_;
};