#include "Fairy.h"


Fairy::Fairy()
{
    load();
    shoot_timer_ = 0;
    health_ = 100;
    going_right_ = true;
}

void Fairy::load()
{
    data_.loadFileImage("player.png");
    data_.setSpriteSheet(128, 128, 2);

    data_.newAnimation("idle", 2, 2, 100);
    // data_.newAnimation("shoot", 3, 4, 100);

    ne::EntityLogic::setRect(ne::Rectf(0,0,40,70));

    data_.setBaseDisplacement(-45,-25,40);
    ne::EntityImage::attach(&data_);

    loadAnimation("idle");

    setPosition(ne::Vector2f(400,300) - ne::Vector2f(data_.getW() / 2.0f, data_.getH() / 2.0f));
    setSpeed(100.0f);

    ne::EntityImage::updateLast();
    ne::EntityLogic::setGravity(0);
}


Fairy::~Fairy()
{
}

void Fairy::Die()
{
}

void Fairy::Shoot(Player &player, const float dt)
{
    //int rand = ne::random(-50, 50);
    if (shoot_timer_ >= 0.35f)
    { 
        bullets_.push_back(new Bullet(ne::Entity::getPosition(), ne::Vector2f(player.getPosition().x - getPosition().x, player.getPosition().y - getPosition().y), 1.0f));
        shoot_timer_ = 0;
    }
    else
    {
        shoot_timer_ += dt;
    }
}

float Fairy::getScoreValue()
{
    return 0;
}

void Fairy::transitionAnimations()
{
}

void Fairy::AI(Player &player, const float dt)
{
    ne::Vector2f direction;
    if ( getPosition().x < 600 && going_right_)
    {
        direction.x = 1.0f;
    }
    else 
    {
        direction.x = -1.0f;
        going_right_ = false;
    }
    if ( getPosition().x < 100)
    {
        going_right_ = true;
    }
    /*
    direction.x = player.getPosition().x - getPosition().x;
    direction.y = player.getPosition().y - getPosition().y;
    */
    Shoot(player, dt);
    ne::Entity::input(direction);
}

void Fairy::takeDamage()
{
    health_ -= 1;
}

void Fairy::updateBullets(const float dt)
{ 
    for (unsigned int i=0; i<bullets_.size(); i++)
    {
        bullets_[i]->logic(dt);
        bullets_[i]->update(dt);
    }
}

void Fairy::drawBullets(ne::Renderer& r)
{
    for (unsigned int i=0; i<bullets_.size(); i++)
        r.draw(*bullets_[i]);
}
