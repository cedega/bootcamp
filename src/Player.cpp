#include "Player.h"

Player::Player()
{

}

Player::~Player()
{
    for (unsigned int i=0; i<bullets_.size(); i++)
        delete bullets_[i];
}

void Player::load()
{
    data_.loadFileImage("player.png");
    data_.setSpriteSheet(128, 128, 2);
    
    data_.newAnimation("idle", 2, 2, 100);
    data_.newAnimation("move", 1, 1, 100);
    
    ne::EntityLogic::setRect(ne::Rectf(0,0,40,70));
    
    data_.setBaseDisplacement(-45, -25, 40);
    ne::EntityImage::attach(&data_);
    
    loadAnimation("idle");

    setPosition(ne::Vector2f(400,300) - ne::Vector2f(data_.getW() / 2.0f, data_.getH() / 2.0f));
    ne::EntityImage::updateLast();
    
    ne::EntityLogic::setGravity(0);
    setSpeed(650.0f);
}

void Player::transitionAnimations()
{

}

void Player::input(const float dt)
{
    const Uint8 *keyboard = SDL_GetKeyboardState(NULL);
    
    ne::Vector2f direction;
    if (keyboard[SDL_SCANCODE_A])
        direction.x -= 1.0f;
    if (keyboard[SDL_SCANCODE_D])
        direction.x += 1.0f;
    if (keyboard[SDL_SCANCODE_W])
        direction.y -= 1.0f;
    if (keyboard[SDL_SCANCODE_S])
        direction.y += 1.0f;
    if (keyboard[SDL_SCANCODE_SPACE])
        shoot();
        
    ne::Entity::input(direction);
}

void Player::shoot()
{
    int rand = ne::random(-100, 100);
    bullets_.push_back(new Bullet(ne::Entity::getPosition(), ne::Vector2f(rand/100.0f, -0.5f), 2.0));
}

void Player::updateBullets(const float dt)
{
    for (int i=0; i<(int)bullets_.size(); i++)
    {
        bullets_[i]->logic(dt);
        bullets_[i]->update(dt);
        
        if (!bullets_[i]->getAlive())
        {
            delete bullets_[i];
            bullets_.erase(bullets_.begin() + i);
            --i;
        }        
        
    }
}

void Player::drawBullets(ne::Renderer& r)
{
    for (unsigned int i=0; i<bullets_.size(); i++)
    {
        r.draw(*bullets_[i]);
        r.draw(bullets_[i]->getRect(), ne::toSDLColor(0,0,0));
    }
    
}
