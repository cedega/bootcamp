#include <Neru/Engine.h>
#include "Player.h"
#include "Enemy.h"
#include "Fairy.h"

class Engine : public ne::Engine
{
public:
    Engine();
    
    void logic(const float dt);
    void render(ne::Renderer&);
    
private:
    Player player_;
    Fairy fairy1_;
};
