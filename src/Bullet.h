#pragma once
#include <Neru/Entity.h>


class Bullet : public ne::Entity
{
public:
    Bullet(const ne::Vector2f &position, const ne::Vector2f &direction, float lifetime);
    virtual ~Bullet() {}
    void load();
    static void init();
    
    void transitionAnimations() {}
    void update(const float dt);
    
    bool getAlive() {return alive_;}
    
private:
    ne::EntityData data_;
    static ne::Image player_bullet_;
    bool alive_;
    float lifetime_;
    float tick_;
};