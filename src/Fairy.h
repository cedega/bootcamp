#pragma once
#include "Enemy.h"
#include "Bullet.h"

class Fairy : public Enemy
{
    public:
        Fairy();
        ~Fairy();
        void AI(Player &player, const float dt);
        void load();
        void Shoot(Player &player, const float dt);
        float getScoreValue();
        void takeDamage(); 
        void transitionAnimations();
        void Die();
        void updateBullets(const float dt);
        void drawBullets(ne::Renderer& r);
    private:
        std::vector<Bullet*> bullets_;
        bool going_right_;
};

