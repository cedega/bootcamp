/*
 * Enemy.h
 *
 * Contains functions and members common to all enemies.
 */

#pragma once
#include <Neru/Entity.h>
#include "Player.h"

class Enemy : public ne::Entity
{
    public:
        Enemy() {}
        virtual ~Enemy() {}
        virtual void Die() = 0;
        virtual void Shoot(Player&, const float dt) = 0;
        virtual float getScoreValue() = 0;
        virtual void load() = 0;
        virtual void takeDamage() = 0;
        virtual void AI(Player &player, const float dt) = 0;
    protected:
        float score_value_;
        float shoot_timer_;
        float health_;
        ne::EntityData data_;
};

