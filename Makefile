#INCLUDE LOCATION
INCLUDE=../neru/include/

ifeq ($(OS),Windows_NT)
	MESSAGE="Operating System Detected: Windows"
	COMPILE=../neru/compile/windows/
	LIBS=-lneru -lmingw32 -lSDL2main -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf -lSDL2_net -lminizip -lbz2 -lz -lm -static-libgcc -static-libstdc++ -Wall
	BINARY=binaries/windows/neru.exe
else
	MESSAGE="Operating System Detected: Linux"
	COMPILE=../neru/compile/linux/
	LIBS=-lneru -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf -lSDL2_net -lminizip -lz -lm -Wall
	BINARY=binaries/linux/neru
endif

srcs = $(wildcard src/*.cpp)
objs = $(srcs:.cpp=.o)
deps = $(srcs:.cpp=.d)
OBJECTS=src/*.o

all: bin

bin: $(objs)
	@echo $(MESSAGE)
	g++ -O2 $(OBJECTS) -I"$(INCLUDE)" -L"$(COMPILE)" $(LIBS) -o $(BINARY)

%.o: %.cpp
	g++ -O2 -I"$(INCLUDE)" -L"$(COMPILE)" -MMD -MP -c $< -o $@ $(LIBS)

clean:
	rm -f src/*.o src/*.d

-include $(deps)
